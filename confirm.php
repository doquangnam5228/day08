<?php
    session_start();
    $img = $_SESSION["img_encode"]
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
    <link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js">
</script>
<style>
body {
    display: flex;
    justify-content: center;
}

.wrapper {
    border: 1px solid #41719c;
    padding-top: 40px;
    padding-right: 100px;
    padding-bottom: 15px;
    padding-left: 25px;
    display: inline-block;
}

.required:after {
    content:"*";
    color: red;
}

label {
    background-color: #70AD47;
    color: white;
    width: 80px;
    padding-left: 10px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-right: 10px;
    border: 1px solid #41719c;
    margin-bottom: 10px;
    display: inline-block;
}

input[type=text2] {
    width: 300px;
    line-height: 37px;
    margin-left: 10px;
    border: 1px solid #41719c;
}

.a {
  display: contents;
  margin-left: 10000px;
  border: 1px solid red;
  background-color: lightgrey;
  padding: 10px;
  width: 10000px;
}

select{
    width: 150px;
    height: 40px;
    line-height: 50px;
    margin-left: 10px;
    border: 1px solid #41719c;
}

input[type=text1]{
    width: 144px;
    line-height: 37px;
    margin-left: 10px;
    border: 1px solid #41719c;
}

input[type=radio]{
    width: auto;
    line-height: 37px;
    margin-left: 10px;
    border: 1px solid #70AD47;
}

input[type=file]{
    width: auto;
    line-height: 37px;
    margin-left: 10px;
}

input[type = submit] {
    width: 120px;
    height: 45px;
    background-color: #70AD47;
    color: white;
    margin-left: 190px;
    border: 1px solid #41719c;
    padding: 5px 25px;
    border-radius: 7px;
    margin-top: 15px;
}

.image_input {
    display: flex;
    align-items: flex-start;

}

.picture {
    display: block;
    width: 30%;
    height: 30%;
}

</style>
<body>
        <div class = "wrapper">
        <form action="" method="POST" enctype="multipart/form-data">
            <div class = "a">
                <label>Họ và tên</label>
                <?php 
                    print_r($_SESSION['name'])
                ?>
            </div>

            <div class = "Gender">
                <label>Giới tính</label>
                <?php 
                    print_r($_SESSION['gender_array'][$_SESSION['gender']])
                ?>
            </div>

            <div class = "khoa">
                <label>Phân khoa</label>
                <?php 
                    print_r($_SESSION['khoa_array'][$_SESSION['khoa']])
                ?>
            </div>

            <div class = "birthday">
                <label>Ngày sinh</label>
                <?php 
                    print_r($_SESSION['birthday'])
                ?>
            </div>

            <div class = "Address">
                <label >Địa chỉ</label>
                <?php
                    if ($_SESSION['address'] == null) {
                        echo "";
                    } else 
                        print_r($_SESSION['address']); 
                ?>
            </div>

            <div class = "image_input">
                <label >Hình ảnh</label>
                <?php 
                    echo '<img class="picture" src="' . $img . '" >';
                ?>
            </div>

            <input type="submit" value="Xác nhận"/></td>
        </form>
        </div>

</body>
</html>